'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class email_status extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  email_status.init({
    emailId: DataTypes.STRING,
    messageId: DataTypes.STRING,
    to: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'email_status',
  });
  return email_status;
};