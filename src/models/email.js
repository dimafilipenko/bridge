'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class email extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    email.init({
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        message_id: DataTypes.STRING,
        emailTo: DataTypes.STRING,
        emailFrom: DataTypes.STRING,
        text: DataTypes.TEXT,
        title: DataTypes.STRING,
        status:DataTypes.STRING,
        sendAt: DataTypes.DATE,

        source_account_id: DataTypes.INTEGER,
        gateway_id: DataTypes.INTEGER,
        html: DataTypes.TEXT,
        attachments: {
            type: DataTypes.TEXT,
            get: function () {
                let res;
                try {
                    res = JSON.parse(this.getDataValue('attachments'))
                } catch (e) {
                    return false;

                }
                return res;
            },
            set: function (value) {
                let res, enc;
                try {
                    enc = JSON.stringify(JSON.parse(value))
                    res = this.setDataValue('attachments', enc)
                } catch (e) {
                    return false;

                }
                return res;
            }
        },
        type: DataTypes.STRING,
        // status: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'email',
    });
    return email;
};