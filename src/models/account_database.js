'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class account_database extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  account_database.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    accountId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'account_database',
  });
  account_database.associate = models => {
    models.account_database.belongsTo(models.source_accounts, {
      as: "database",
      foreignKey: "accountId",
      // otherKey: "accountId" // replaces `categoryId`
    });
  }
  return account_database;
};