'use strict';
// import {hash} from "bcrypt";

let hat = require("hat")
let bcrypt = require("bcrypt")

const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class source_accounts extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    source_accounts.init({
        name: {
            allowNull: false,
            type: DataTypes.STRING
        },
        role: {
            type: DataTypes.STRING
        },
        api_key: {

            type: DataTypes.TEXT
        },
        email: {
            allowNull: false,
            type: DataTypes.STRING
        },
        password: {
            type: DataTypes.STRING,
            set(value) {
                // Storing passwords in plaintext in the database is terrible.
                // Hashing the value with an appropriate cryptographic hash function is better.
                // Using the username as a salt is better.
                let saltRounds = 10
                const salt = bcrypt.genSaltSync(saltRounds);
                this.setDataValue('password', bcrypt.hashSync(value, salt));
            }
        },
        webhook_url: {
            allowNull: true,
            type: DataTypes.STRING
        },
        default_type: DataTypes.STRING,
        default_gateway_id: DataTypes.NUMBER
    }, {
        sequelize,
        modelName: 'source_accounts',
        hooks: {
            beforeCreate: function (source_accounts, options) {
                source_accounts.api_key = hat()
            },


        }
    });
    source_accounts.associate = models => {
        models.source_accounts.belongsToMany(models.Gateways, {
            through: "account_gateways",
            as: "gtws",
            foreignKey: "accountId",
            // otherKey: "accountId" // replaces `categoryId`
        });
        models.source_accounts.hasMany(models.account_database, {
            as: "database",
            foreignKey: "accountId",
            // otherKey: "accountId" // replaces `categoryId`
        });
    }
    return source_accounts;
};