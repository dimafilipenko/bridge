'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class message_status extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    message_status.init({
        messageId: DataTypes.STRING,
        refId: DataTypes.STRING,
        number: DataTypes.STRING,
        status: DataTypes.STRING,
        parts: DataTypes.INTEGER,
        country: DataTypes.STRING,
        cost: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'message_status',
    });
    return message_status;
};