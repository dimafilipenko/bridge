const jwt = require('jsonwebtoken');
const {JWTSECRET} = require("../config/env");
const db = require("../models");

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, JWTSECRET, async (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }
            req.user = await db.source_accounts.findOne({
                where: {
                    id: user.id
                }
            })
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

module.exports = authenticateJWT
