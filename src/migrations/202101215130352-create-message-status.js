'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('message_statuses', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            refId: {
                type: Sequelize.STRING
            },
            messageId: {
                type: Sequelize.STRING
            },
            number: {
                type: Sequelize.STRING
            },
            status: {
                type: Sequelize.STRING
            },
            country: {
                type: Sequelize.STRING
            },
            cost: {
                type: Sequelize.STRING
            },
            parts: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('message_statuses');
    }
};