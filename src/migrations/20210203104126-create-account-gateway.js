'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('account_gateways', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      gatewayId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Gateways', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },

        onDelete: 'CASCADE',
      },
      accountId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'source_accounts', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },

        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('account_gateways');
  }
};