const helpers = require('../helpers/helpers');
const {body, check, validationResult} = require('express-validator');
let validator = require('validator');
const avaliable_api = require('../config/avaliable_api');
const Sequelize = require("sequelize");
module.exports = (app, db) => {
    let route = "/api/message"
    app.get(route + "/getStatus", [
            body('message_id').notEmpty(),
        ], (req, res) => {
            let api_key = req.body.api_key || req.query.api_key
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            let ids = req.body.message_id.split(',');

            // console.log(ids)
            if (!helpers.check_source_accounts_api_key(api_key, res, db)) {
                return res.status(400).send("error api_key")

            }
            helpers.get_source_accounts_by_api_key(db, api_key, async (source_account) => {
                let result = []
                if (!source_account)
                    return res.status(400).json({errors: [{'msg': "wrong api_key"}]});
                result = await getStatus(req, {
                    id: ids,
                    source_account_id: source_account.id
                })

                if (result && result.length)
                    return res.json(result)
                else return res.status(400).json({err: "no data"})
            })
        }
    );

    app.post(route + "/send", [
            body('attachments').isJSON(),
        ], (req, res) => {
            let api_key = req.body.api_key || req.query.api_key


            helpers.get_source_accounts_by_api_key(db, api_key, async (source_account) => {
                if (!source_account)
                    return res.status(400).json({errors: [{'msg': "wrong api_key"}]});
                let message = req.body
                if (!message.emailFrom) {
                    message.emailFrom = source_account.email
                }

                message.status = "not_sent_yet"

                message.source_account_id = source_account.dataValues.id
                message.gateway_id = source_account.default_gateway_id
                let gateway;
                if (req.body.gateway_name) {
                    gateway = await db.Gateways.findOne({
                        where: {
                            name: req.body.gateway_name
                        }
                    })
                } else {
                    gateway = await db.Gateways.findOne({
                        where: {
                            id: message.gateway_id
                        }
                    })
                }

                if (gateway) {
                    if (!req.body.type)
                        message.type = gateway.type
                    if (req.body.type) {
                        if (gateway.type !== req.body.type)
                            return res.status(400).json({
                                errors:
                                    [{
                                        'msg': `gateway ${gateway.name} doesn\`t has ${req.body.type} type,` +
                                            +`recommended type: ` + gateway.type
                                    }]
                            });

                    }
                    message.gateway_id = gateway.id
                    message.api_name = gateway.api_name
                } else return res.status(400).json({errors: [{'msg': "this gateway does not exist"}]});

                // console.log(message)

                await createMessage(message, req, res)

            })

        }
    );


    app.get("/api/messages/getStatus", [], (req, res) => {
            let api_key = req.body.api_key || req.query.api_key

            if (!helpers.check_source_accounts_api_key(api_key, res, db)) {
                return res.status(400).send("error api_key")
            }
            helpers.get_source_accounts_by_api_key(db, api_key, async (source_account) => {
                let result = []
                if (!source_account)
                    return res.status(400).json({errors: [{'msg': "wrong api_key"}]});
                result = await getStatus(req, {source_account_id: source_account.id})
                console.log(result)
                if (result)
                    return res.json(result)
                else return res.status(400).json({err: "no data"})
            })
        }
    );

    async function createMessage(message, req, res) {


        let required = await helpers.checkRequiredOptions(Object.keys(message), avaliable_api[message.api_name]["message"]["req"])
        console.log(required)
        let numbers;
        if (required) {
            return res.status(400).json({
                errors: [{
                    'msg': `struct message for ${message.api_name} is wrong (${required} is required)`,
                    [message.api_name]: avaliable_api[message.api_name]["message"]
                }]
            });
        }
        if (message.type === "sms" || message.type === "viber") {

            if (message.number) {
                numbers = await split_numbers(message.number)
                message.number = numbers.join(',')
            }
            if (!message.number)
                return res.status(400).json({errors: [{'msg': "number format is wrong"}]});
            if (!message.gateway_id)
                return res.status(400).json({errors: [{'msg': "gateway_name or default_gateway is not correct"}]});
        }
        if (message.type === "sms") {
            // console.log(message)

            // const emailRes = await db.email.create(message)

            const SmsRes = await db.sms.create(message);
            for (const number in numbers) {
                await db.message_status.create({
                    number: numbers[number],
                    status: "ok",
                    refId: SmsRes.id,
                }).catch(error => console.log(error));
            }
            return res.json(SmsRes)
        } else if (message.type === "viber") {
            const viberRes = await db.viber.create(message);
            for (const number in numbers) {
                await db.message_status.create({
                    number: "ok",
                    status: message.status,
                    refId: viberRes.id,
                }).catch(error => console.log(error));
            }
            return res.json(viberRes)
        } else if (message.type === "email") {

            // if (!validator.isEmail(message.emailTo)) {
            //     return res.status(400).json({errors: [{'msg': "emailTo: '" + message.emailTo + "' is not email"}]});
            // }
            if (!validator.isEmail(message.emailFrom)) {
                return res.status(400).json({errors: [{'msg': "emailFrom: '" + message.emailFrom + "' is not email"}]});
            }
            let emails = await split_to(message.emailTo)
            message.emailTo = emails.join(',')

            const emailRes = await db.email.create(message)
            for (const email in emails) {
                await db.email_status.create({
                    to: emails[email],
                    status: "ok",
                    emailId: emailRes.id,
                }).catch(error => console.log(error));
            }

            return res.json(emailRes)
        } else {
            res.status = 400
            res.send({"status": "error", "err": "type is wrong or empty"})
            return false
        }
    }

    async function getStatus(req, whereOption) {
        let sms = [];
        let emails = [];
        let viber = [];
        let result = [];

        db.message_status.belongsTo(db.viber,
            {
                foreignKey: 'refId'

            })
        db.viber.hasMany(db.message_status, {
            foreignKey: 'refId'

        })
        viber = await db.viber.findAll({
            // attributes: ['id', "type", "createdAt", "updatedAt"],
            where: whereOption,
            include: [
                {
                    model: db.message_status,
                    attributes: ['number', 'country', 'cost', 'status', 'createdAt', 'updatedAt'],
                    required: false
                }
            ]
        })

        db.message_status.belongsTo(db.sms,
            {


                    foreignKey: 'refId'

            })
        db.sms.hasMany(db.message_status, {

            foreignKey: 'refId'

        })
        sms = await db.sms.findAll({
            // attributes: ['id', "type", "createdAt", "updatedAt"],
            where: whereOption,
            include: [
                {
                    model: db.message_status,
                    attributes: ['number', 'country', 'cost', 'status', 'createdAt', 'updatedAt'],
                    required: false
                }
            ]
        })

        db.email_status.belongsTo(db.email)
        db.email.hasMany(db.email_status, {
            foreignKey: {
                name: 'emailId'
            }
        })
        emails = await db.email.findAll({
            attributes: ['id', "type", "emailFrom", "status", "sendAt", "createdAt", "updatedAt"],
            where: whereOption,
            include: [
                {
                    model: db.email_status,
                    attributes: ['to', 'status', 'createdAt', 'updatedAt'],
                    required: false
                }
            ]
        })

        if (req.body.type === "viber")
            result = viber
        else if (req.body.type === "sms")
            result = sms
        else if (req.body.type === "email")
            result = viber
        else {
            result = {
                "viber": viber,
                "sms": sms,
                "emails": emails,

            }
        }
        return result;
    }

    function IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    function split_to(str) {
        let email_Arr = str.split(",");

        email_Arr.forEach((email, index) => {
                if (email && validator.isEmail(email.trim())) {

                    email_Arr[index] = email.trim()

                } else
                    email_Arr.splice(index, 1);
            }
        );
        return email_Arr
    }

    function split_numbers(str) {
        let phone_Arr = str.split(",");

        phone_Arr.forEach((phone, index) => {
            if (phone)
                phone_Arr[index] = validate_number(phone.trim())
            else
                phone_Arr.splice(index, 1);
        });

        return phone_Arr

    }

    function validate_number(phone) {

        return phone.toString().replace("+", "");

    }

}