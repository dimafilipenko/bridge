const nodemailer = require("nodemailer");
const db = require("../models");
const https = require('https');

// async..await is not allowed in global scope, must use a wrapper
class sendSmtpController {
    result
    attachments

    sendWithAttachments(message) {
        return new Promise(async (resolve, reject) => {
            await db.email.findOne({
                where: {
                    id: message.id
                }
            }).then(async mail => {
                this.attachments = mail.get("attachments")
                console.log(this.attachments)
                let newAttachments = []
                for (const attachment in this.attachments) {
                    let newAtt = await this.createAttachForSmtp(this.attachments[attachment])
                    if (newAtt)
                        newAttachments.push(newAtt)
                }
                message.attachments = newAttachments
                console.log(message.attachments[3])
                this.sendMail(message).then(b => resolve(b))
            }).catch(e => {
                reject(e)
            })
        })
    }

    createAttachForSmtp(attach) {
        return new Promise((resolve, reject) => {
            let body = [];
            let pathToAttachment = new URL(attach.url);
            let req = https.request(pathToAttachment, (response) => {


                response.on('data', chunk => body.push(chunk));
                response.on('end', (v) => {

                    let att = {}
                    try {
                        body = (Buffer.concat(body).toString("base64"))
                        att.filename = attach.filename
                        att.content = body
                        att.contentType = response.headers["content-type"]
                        att.encoding = 'base64'

                        // att.disposition = "attachment"
                    } catch (e) {
                        reject(e)
                    }
                    resolve(att)
                });
            })
            req.on('error', function (err) {
                // handle errors with the request itself
                resolve(false)
                console.error('Error with the request:', err.message);
            });
            req.end()
        })
    }

    async sendMail(message) {
        return new Promise(async (resolve, reject) => {
            // let testAccount = await nodemailer.createTestAccount();
            const gateway = await db.Gateways.findOne({
                where: {
                    id: message.gateway_id
                }
            })
            // console.log(gateway.get('auth'))
            const source_account = await db.source_accounts.findOne({
                where: {
                    id: message.source_account_id
                }
            })
// create reusable transporter object using the default SMTP transport

            let transporter = nodemailer.createTransport({
                host: gateway.get('auth').host,
                port: gateway.get('auth').port || 587,
                secure: false, // true for 465, false for other ports
                auth: gateway.get('auth')//user pass
            });
        console.log(message.emailTo)
            this.split_to(message.emailTo).forEach(email => {
                let msg = {
                    from: message.emailFrom || source_account.email, // sender address
                    to: email, // list of receivers
                    subject: message.title, // Subject line
                    text: message.text, // plain text body
                    html: message.html, // html body

                }
                if (message.attachments)
                    msg.attachments = message.attachments
                transporter.sendMail(msg).then(i => {
                    if (i) {

                        this.result = i
                        let message_id = this.result.messageId
                        db.email_status.update(
                            {
                                status: "delivered",
                                messageId: message_id
                            }, {
                                where: {
                                    to: email,
                                    emailId: message.id
                                }
                            });
                        resolve(this.result)
                        console.log(i)
                    }
                }).catch(err => {
                    this.result = err
                    db.email_status.update(
                        {
                            status: "rejected",
                        }, {
                            where: {
                                to: email,
                                emailId: message.id
                            }
                        });
                    console.log(email)
                    reject(this.result)
                });
            })
            return this.result
        })
    }

    async send(message) {

        if (message.attachments) {
            return this.sendWithAttachments(message)
        } else {
            return await this.sendMail(message)
        }

    }



    split_to(str) {
        let email_Arr = str.split(",");

        email_Arr.forEach((email, index) => {
            if (email)
                email_Arr[index] = email.trim()
            else
                delete email_Arr[index]
        });
        return email_Arr
    }


}

module
    .exports
    .sendSmtp = sendSmtpController