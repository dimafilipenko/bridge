const {body, query, validationResult} = require('express-validator');
module.exports = (app, db) => {

    let CallbackRouteIntelTele = "/api/callback/intel-tele"

    app.post(CallbackRouteIntelTele,
        async (req, res) => {
            console.log(req.body)
            console.log("////////////////////////////")
            const message = db.message_status.findOne({where: {messageId: req.body.msgid}})
                .then( async function  (Message) {
                    // Check if record exists in db
                    if (Message) {
                        Message.update({
                            status: req.body.status
                        })
                        const pocketViber = await db.viber.findOne({
                            where: {
                                id: Message.refId
                            }
                        })
                        if (pocketViber) {
                            pocketViber.status = "Ok";
                            await pocketViber.save();
                        }
                        const pocketSMS = await db.sms.findOne({
                            where: {
                                id: Message.refId
                            }
                        })
                        if (pocketSMS) {
                            pocketSMS.status = "Ok";
                            await pocketSMS.save();
                        }
                    }
                })

            res.status(200).send()
        })
    let callBackSendGrid = "/api/callback/sendgrid"

    app.post(callBackSendGrid,
        async (req, res) => {
            let items = req.body;
            const updateSendgrid = async _ => {
                let count = items.length;
                for (let i = 0; i < count; i++) {
                    let message_id = items[i].sg_message_id.split('.');
                    let em = await db.email_status.findOne({
                        where: {
                            to: items[i].email,
                            messageId: message_id[0]
                        }
                    }).catch(err => console.log(err));
                    if (em) {
                        em.status = items[i].event
                        await em.save();
                        let result = await db.email.findOne({
                            where: {
                                id: em.emailId
                            }
                        }).catch(err => console.log(err));
                        if (result) {
                            result.message_id = em.emailId
                            result.status = 'ok'

                            await result.save();
                            console.log(result)
                        }
                    }

                }
            }
            await updateSendgrid();
            res.status(200).send()
        })
    let testCallbackSourceAccount = "/api/callback/test"

    // app.post(testCallbackSourceAccount,
    //     (req, res) => {
    //         console.log("/api/callback/test")
    //         // console.log(req.body)
    //         res.status(200).send()
    //     })
}