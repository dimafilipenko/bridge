const axios = require('axios');
const db = require("../models");

class intelteleController {

    /**
     *
     * @param number number to send
     * @param from name from
     * @param message
     */
    viber = {}
    sms = {}

    reply = undefined
    message_id = undefined
    message = undefined


    async createMessage(message) {
        this.message = message
        let source_account = await db.source_accounts.findOne({
            where: {
                id: message.source_account_id
            }
        })
        this.viber.to = this.sms.to = message.number;
        this.sms.from = this.viber.im_sender = source_account.name;
        this.sms.message = this.viber.im_message = message.text;
    }

    auth(auth) {
        this.sms.username = this.viber.username = auth.username
        this.sms.api_key = this.viber.api_key = auth.api_key
    }

    async sendViber(image, button_text, button_link, ttl) {
        this.viber.im_image = image
        this.viber.im_button_text = button_text
        this.viber.im_button_link = button_link
        this.viber.im_ttl = ttl
        // console.log(this.viber)

        await axios.get('http://api.sms.intel-tele.com/im/send/', {params: this.viber}).then((res) => {

                this.create_message_statuses(res.data.reply)
                this.reply = true;

            }
        ).catch(error => {
            this.reply = false
        });
        console.log(this.reply)
        return this.reply
    }

    async sendSms(priority, system_type) {
        this.sms.priority = priority
        this.sms.system_type = system_type
        // console.log(this.sms)


        await axios.get('http://api.sms.intel-tele.com/message/send/', {params: this.sms}).then((res) => {
                this.create_message_statuses(res.data.reply)
                this.reply = true;


            }
        ).catch(error => {
            this.reply = error.response.data
            this.reply = false;
        });


        return this.reply
    }

    async create_message_statuses(reply) {
        console.log(reply)
        for (let item in reply) {
            db.message_status.update(
                {
                    status: reply[item].status,
                    messageId: reply[item].message_id,
                    country: reply[item].country,
                    cost: reply[item].cost,
                    parts: reply[item].parts,
                }, {
                    where: {
                        number: reply[item].number,
                        refId: this.message.id
                    }
                });

        }
    }

    async check_status(message_ids) {
        if (message_ids) {
            if (message_ids === 'object')
                message_ids.join(',')
            this.sms.requests = message_ids
            await axios.get("http://api.sms.intel-tele.com/message/status/", {params: this.sms})
        }
        return false
    }


}

module
    .exports
    .intelteleController = intelteleController;
