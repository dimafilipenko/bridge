const {body, query, validationResult} = require("express-validator");
const helpers = require("../helpers/helpers");
const authenticateJWT = require("../middleware/authMiddleware");
const bcrypt = require("bcrypt");
let hat = require("hat")
const jwt = require("jsonwebtoken");
const {JWTSECRET} = require("../config/env");

module.exports = (app, db) => {
    let single = "/api/source_account";
    let plural = "/api/source_accounts";
    /**
     * admin
     */
    app.post("/api/login",
        [
            body("password").notEmpty(),
            body("email").notEmpty(),
        ],
        async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            // Read username and password from request body
            const {email, password} = req.body;

            // Filter user from the users array by username and password
            const user = await db.source_accounts.findOne({
                where: {
                    email: email,
                },

            }).catch(err => {
                    res.status(403).send("No User With this Email");
                }
            );
            if (!user)
                return res.status(403).send("No User With this Email");
            console.log(password, user.password)
            bcrypt.compare(password, user.password, function (err, isMatch) {
                if (err) {
                    // handle error
                }
                if (isMatch) {
                    const accessToken = jwt.sign({id: user.id, username: user.username, role: user.role}, JWTSECRET);
                    let fields = user.dataValues;
                    delete fields.password;
                    return res.json({
                        accessToken,
                        "user": fields
                    });
                } else {
                    // response is OutgoingMessage object that server response http request
                    res.status(403).send("Wrong Password");
                }
            });

        }
    )
    ;
    app.get(plural, authenticateJWT, async (req, res) => {
            let users = await db.source_accounts.findAll({
                include: "gtws",
                attributes: ["id", "name", "email", "role", "api_key", "createdAt", "updatedAt"],
            }).catch(function (err) {
                res.status(403).send("Unauthorizated");
            })
            res.json(users)
        }
    );
    app.post(single + "/:id/api_key_refresh", authenticateJWT, async (req, res) => {
            const user = await db.source_accounts.findOne({
                where: {
                    id: req.params.id,
                },
            }).catch(err => {
                res.status(403).send("no user with this id");
            })
            if (user.id === req.user.id || req.user.role === "admin") {
                user.api_key = hat();
                await user.save();
                res.json(user);

            } else {
                res.status(403).send("Unauthorizated");
            }
        }
    );
    /**
     * admin
     */
    app.get(single + "/:id", authenticateJWT, (req, res) => {
            const role = req.user.role;
            if (role === "admin")
                db.source_accounts.findOne({
                    where: {
                        id: req.params.id
                    },
                    include: {
                        model: db.Gateways,
                        as: "gtws"
                    },
                }).then((result) => {

                    res.json(result);
                }).catch(function (err) {
                    res.send(err.parent.sqlMessage);
                })
            else res.sendStatus(403);
        }
    );
    /**
     * service
     */
    app.get(single, async (req, res) => {
            // const {role} = req.user;
            const user = await db.source_accounts.findOne({
                where: {
                    api_key: req.query.api_key
                },
                include: {
                    model: db.Gateways,
                    as: "gtws"
                }
            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error api_key"]});
            })
            console.log(user)
            if (user) {
                delete user.dataValues.password;
                return res.send(user);
            }
            return res.status(400).json({errors: ["user doesn`t exist"]});

        }
    );
    /**
     * admin
     */
    app.put(single, [
            body("name").notEmpty(),
            body("email").notEmpty(),
            body("role").notEmpty(),
        ], async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty())
                return res.status(400).json({errors: errors.array()});


            const options = {
                name: req.body.name,
                email: req.body.email,
                webhook_url: req.body.webhook_url,
                role: req.body.role,
            }
            const account = await db.source_accounts.findOne({
                where: {
                    api_key: req.query.api_key
                },
            });
        if (req.body.gatewaysIds) {
            const gatewaysIds = req.body.gatewaysIds.split(',');
            const gtws = await db.Gateways.findAll({
                    where: {
                        id: {
                            [db.Sequelize.Op.notIn]: gatewaysIds
                        }
                    }
                }
            );
            const map = gtws

            console.log(map);
            await account.removeGtws(map); // 2 relationship are in db postId: 1 with userId: 2,3
            await account.addGtws(gatewaysIds);
        }
            if (req.body.password)
                options.password = req.body.password
            db.source_accounts.update(options,
                {
                    where: {
                        api_key: req.query.api_key
                    },

                    returning: true,
                }).then(async (result) => {
                const account = await db.source_accounts.findOne({
                    where: {
                        api_key: req.query.api_key
                    },
                    include: {
                        model: db.Gateways,
                        as: "gtws"
                    },
                })

                // account.setGateways();

                // result.save();
                res.json(account)
            });


        }
    );
    app.post(single, authenticateJWT, [
            body("name").notEmpty(),
            body("email").notEmpty(),
            body("password").notEmpty(),
            body("role").notEmpty(),
        ], async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty())
                return res.status(400).json({errors: errors.array()});
            if (req.user.role === "admin")
                db.source_accounts.create({
                    name: req.body.name,
                    email: req.body.email,
                    webhook_url: req.body.webhook_url,
                    role: req.body.role,
                    password: req.body.password,
                }).then((result) => res.json(result)).catch(function (err) {
                    res.send(err.parent.sqlMessage);
                })
            else res.sendStatus(403);

        }
    );
    /**
     * admin
     */
    app.put(single + "/:id", authenticateJWT, [
            body("name").notEmpty(),
            body("email").notEmpty(),
        ], (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            db.source_accounts.findOne({
                where: {
                    id: req.params.id
                }, include: "gtws",
            }).catch(err => console.log(err)).then(async res => {
                if (req.body.gatewaysIds) {
                    const gatewaysIds = req.body.gatewaysIds.split(',');
                    const gtws = await db.Gateways.findAll({
                            where: {
                                id: {
                                    [db.Sequelize.Op.notIn]: gatewaysIds
                                }
                            }
                        }
                    );
                    const map = gtws

                    console.log(map);
                    await res.removeGtws(map); // 2 relationship are in db postId: 1 with userId: 2,3
                    await res.addGtws(gatewaysIds);
                }
            });
            console.log(req.body.gatewaysIds)


            db.source_accounts.update({
                    name: req.body.name,
                    email: req.body.email,
                    webhook_url: req.body.webhook_url,
                    password: req.body.password,
                    role: req.body.role,
                },
                {
                    where: {
                        id: req.params.id
                    }
                }).then((result) => {

                res.json(result)
            });
        }
    );
    /**
     * admin
     */
    app.delete(single + "/:id", authenticateJWT, (req, res) => {
            if (req.user.role === "admin")
                db.source_accounts.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then((result) => res.json(result)).catch(error => {
                    res.json(error)
                });
            else res.sendStatus(403);
        }
    );
}