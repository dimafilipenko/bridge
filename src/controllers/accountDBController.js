const {body, query, validationResult} = require("express-validator");
const helpers = require("../helpers/helpers");
const authenticateJWT = require("../middleware/authMiddleware");
const bcrypt = require("bcrypt");
let hat = require("hat")
const jwt = require("jsonwebtoken");
const {Op} = require("sequelize");
const {JWTSECRET} = require("../config/env");
module.exports = (app, db) => {
    let single = "/api/account_db";
    let plural = "/api/accounts_db";

    /**
     * admin
     */

    app.get(single, async (req, res) => {
            // const {role} = req.user;
            const user = await db.source_accounts.findOne({
                where: {
                    api_key: req.query.api_key
                },
                include: {
                    model: db.account_database,
                    as: "database"
                },

            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error api_key"]});
            })
            // console.log(user)


            return res.json(user);

        }
    );
    app.get(plural, authenticateJWT, async (req, res) => {
            // const {role} = req.user;
            if (req.user.role === 'admin') {
                const user = await db.source_accounts.findAll({
                    include: {
                        model: db.account_database,
                        as: "database"
                    },

                }).catch(error => {
                    console.log(error)
                    return res.status(400).json({errors: ["error api_key"]});
                })
                return res.json(user);
            } else
                return res.status(403);

            // console.log(user)


        }
    );
    app.post(single, async (req, res) => {
            const user = await db.source_accounts.findOne({
                where: {
                    api_key: req.query.api_key
                },
            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error api_key"]});
            })
            let account_db = {};
            account_db.name = req.body.name;
            if (req.body.email)
                account_db.email = req.body.email;
            if (req.body.phone)
                account_db.phone = req.body.phone;
            account_db.accountId = user.id;
            let database = await db.account_database.findOne({
                where: account_db
            });
            if (!database)
                db.account_database.create({
                    // set the default properties if it doesn't exist
                    name: req.body.name,
                    email: req.body.email,
                    phone: req.body.phone,
                    accountId: user.id

                }).then(function (result) {
                    let user = result[0]; // the instance of the author
                    return res.send(user);
                });
            else
                return res.status(400).send(
                    {
                        error: "user with this phone or email already exist"
                    });

        }
    );
    app.get(single + "/:id", authenticateJWT, async (req, res) => {
            const user = await db.source_accounts.findOne({
                where: {
                    id: req.params.id
                },
                include: {
                    model: db.account_database,
                    as: "database"
                },
            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error to find user"]});
            })
            if (user)
                return res.json(user);
            else return res.status(400).json({errors: ["no user with this id"]});
        }
    );
    app.post(single + "/:id", authenticateJWT, async (req, res) => {
            if (req.user.role === "admin") {
                let account_db = {};
                account_db.name = req.body.name;
                if (req.body.email)
                    account_db.email = req.body.email;
                if (req.body.phone)
                    account_db.phone = req.body.phone;
                account_db.accountId = req.user.id;
                let database = await db.account_database.findOne({
                    where: account_db
                });
                if (!database)
                    db.account_database.create({
                        // set the default properties if it doesn't exist
                        name: req.body.name,
                        email: req.body.email,
                        phone: req.body.phone,
                        accountId: req.params.id

                    }).then(function (result) {
                        return res.send(result);
                    });
                else
                    return res.status(400).send(
                        {
                            error: "user with this phone or email already exist",
                        });
            } else res.sendStatus(403);
        }
    );
    app.delete(single + "/:account_id/:id", authenticateJWT, async (req, res) => {
            await db.account_database.destroy({
                where: {
                    accountId: req.params.account_id,
                    id: req.params.id
                },
            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error"]});
            })
            return res.send(true);
        }
    );

    app.delete(single + "/:id", async (req, res) => {
            // const {role} = req.user;
            const user = await db.source_accounts.findOne({
                where: {
                    api_key: req.query.api_key
                },

            }).catch(error => {
                console.log(error)
                return res.status(400).json({errors: ["error api_key"]});
            }).then(async res => {
                await db.account_database.destroy({
                    where: {
                        accountId: res.id,
                        id: req.params.id
                    },
                }).catch(error => {
                    console.log(error)
                    return res.status(400).json({errors: ["error"]});
                })

            })
            return res.status(200)
            // console.log(user)
        }
    );
    // app.delete(single, async (req, res) => {
    //         // const {role} = req.user;
    //         const user = await db.source_accounts.findOne({
    //             where: {
    //                 api_key: req.query.api_key
    //             }
    //
    //         }).catch(error => {
    //             console.log(error)
    //             return res.status(400).json({errors: ["error api_key"]});
    //         })
    //         // console.log(user)
    //
    //
    //         return res.json(user);
    //
    //     }
    // );
    /**
     * service
     */
}