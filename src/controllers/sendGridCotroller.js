const sgMail = require('@sendgrid/mail');
const db = require("../models");
const https = require('https');

// const mime = require('mime-types')

class sendGrid {
    mail = sgMail
    res = undefined
    attachments = undefined
    message

    auth(auth) {
        this.mail.setApiKey(auth.api_key);
    }

    send_with_attach(message) {
        return new Promise(async (resolve, reject) => {
            await db.email.findOne({
                where: {
                    id: message.id
                }
            }).then(async mail => {
                this.attachments = mail.get("attachments")
                // console.log(this.attachments)
                let newAttachments = []
                for (const attachment in this.attachments) {
                    let newAtt = await this.s(this.attachments[attachment])
                    if (newAtt)
                        newAttachments.push(newAtt)
                }
                message.attachments = newAttachments
                console.log(message.attachments[3])
                this.sendEmail(message).then(b => {
                    console.log("REJECT TUT")
                    db.email_status.update(
                        {
                            status: "rejected",
                        }, {
                            where: {
                                status: "not_sent_yet",
                                emailId: message.id
                            }
                        });
                    resolve(b)
                })
            }).catch(e => {
                reject(e)
            })
        })


    }

    async s(attach) {
        return new Promise((resolve, reject) => {
            let body = [];
            let pathToAttachment = new URL(attach.url);
            let req = https.request(pathToAttachment, (response) => {


                response.on('data', chunk => body.push(chunk));
                response.on('end', (v) => {

                    let att = {}
                    try {
                        body = (Buffer.concat(body).toString("base64"))
                        att.filename = attach.filename
                        att.content = body
                        att.type = response.headers["content-type"]
                        att.disposition = "attachment"
                    } catch (e) {
                        reject(e)
                    }
                    resolve(att)
                });


            })
            req.on('error', function (err) {
                // handle errors with the request itself
                resolve(false)
                console.error('Error with the request:', err.message);
            });
            req.end()
        })
    }

    sendEmail(message) {
        return new Promise((resolve, reject) => {

            db.source_accounts.findOne({
                where: {
                    id: message.source_account_id
                }
            }).then(source_account => {
                this.split_to(message.emailTo).forEach(email => {
                    let msg = {

                        to: email,
                        from: message.emailFrom || source_account.email, // Use the email address or domain you verified above
                        subject: message.title,
                        text: message.text,
                        html: message.html,
                    };
                    if (message.attachments)
                        msg.attachments = message.attachments
                    this.mail
                        .send(msg)
                        .then((res) => {
                            this.res = res[0].headers
                            let message_id = this.res["x-message-id"]
                            console.log(res)
                            db.email_status.update(
                                {
                                    status: "OK",
                                    messageId: message_id
                                }, {
                                    where: {
                                        to: email,
                                        emailId: message.id
                                    }
                                });


                        }, error => {
                            console.error(error);
                            this.res = false
                            db.email_status.update(
                                {
                                    status: "rejected",
                                }, {
                                    where: {
                                        to: email,
                                        emailId: message.id
                                    }
                                });
                            reject(this.res)
                            if (error.response) {
                                console.error(error.response.body)
                            }
                        });


                })
            })

            resolve(this.res)

        })
    }

    async send(message) {
        if (Object.keys(message).includes("attachments")) {
            return this.send_with_attach(message)

        } else
            return this.sendEmail(message)

    }

    split_to(str) {
        let email_Arr = str.split(",");

        email_Arr.forEach((email, index) => {
            if (email)
                email_Arr[index] = email.trim()
            else
                delete email_Arr[index]
        });
        return email_Arr
    }


}

module.exports.sendGrid = sendGrid
