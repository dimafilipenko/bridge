const helpers = require('../helpers/helpers');
const {body, validationResult} = require('express-validator');
const avaliable_api = require('../config/avaliable_api');
const authenticateJWT = require("../middleware/authMiddleware");

module.exports = (app, db) => {

    let single = "/api/gateway"
    let plural = "/api/gateways"

    app.get(plural, [], (req, res) => {

            db.Gateways.findAll({
                attributes: ["id", "name", "type", "api_name", "createdAt"]
            }).then((result) => res.json(result)).catch(function (err) {
                res.send(err.parent.sqlMessage)
            })
        }
    );

    app.get(single + "/:id", authenticateJWT, (req, res) => {

        if (req.user.role === "admin")
                db.Gateways.findByPk(req.params.id).then((result) => res.json(result)).catch(function (err) {
                    res.send(err.parent.sqlMessage)
                })

        }
    );

    app.post(single, authenticateJWT, [
            body('auth').notEmpty(),
            body('name').notEmpty(),
            body('api_name').notEmpty(),
            body('type').isIn(["viber", "sms", "email"]),
            body('auth').isJSON(),
        ], async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            if (req.user.role === "admin") {
                if (!helpers.check_api_name(req.body.api_name))
                    return res.status(400).send({"status": "error", "err": "NO such api_name"})
                let required = await helpers.checkRequiredOptions(Object.keys(JSON.parse(req.body.auth)), avaliable_api[req.body.api_name]["auth"])
                if (required) {
                    let api_name = req.body.api_name
                    return await res.status(400).json({
                        errors: [{
                            'msg': `struct auth for ${api_name} is wrong`,
                            [api_name]: avaliable_api[req.body.api_name]["auth"]
                        }]
                    });
                }

                let gateway = new db.Gateways()
                gateway.set("auth", req.body.auth)
                gateway.set("type", req.body.type)
                gateway.set("name", req.body.name)

                gateway.set("api_name", req.body.api_name)
                gateway.save().then((result) => {
                    // console.log(result)
                    res.send(result)
                }).catch(function (err) {
                    res.status(404).send(err)
                })

            }
        }
    );

    app.put(single + "/:id", authenticateJWT, [
            body('auth').notEmpty(),
            body('name').notEmpty(),
            body('api_name').notEmpty(),
            body('type').isIn(["viber", "sms", "email"]),
        ], async (req, res) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            if (req.user.role === "admin") {
                let gateway = await db.Gateways.findOne({
                    where: {
                        id: req.params.id
                    }
                })
                let required = await helpers.checkRequiredOptions(Object.keys(JSON.parse(req.body.auth)), avaliable_api[req.body.api_name]["auth"])
                if (required) {
                    let api_name = req.body.api_name
                    return await res.status(400).json({
                        errors: [{
                            'msg': `struct auth for ${api_name} is wrong`,
                            [api_name]: avaliable_api[req.body.api_name]["auth"]
                        }]
                    });
                }
                gateway.set("auth", req.body.auth)
                gateway.set("type", req.body.type)
                gateway.set("name", req.body.name)
                gateway.set("api_name", req.body.api_name)
                gateway.save().then((result) => {
                    // console.log(result)
                    res.send(result)
                }).catch(function (err) {
                    res.status(404).send(err)
                })

            }
        }
    );

    app.delete(single + "/:id", authenticateJWT, (req, res) => {

            if (req.user.role === "admin")
                db.Gateways.destroy({
                    where: {
                        id: req.params.id
                    }
                }).then((result) => res.json(result)).catch(function (err) {
                    res.send(err.parent.sqlMessage)
                })
        }
    );
}