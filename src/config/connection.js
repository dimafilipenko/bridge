const mysql = require("mysql");
const dbConfig = require("./db.config");
const dotenv = require('dotenv');
const result = dotenv.config();
if (result.error) {
    throw result.error;
}
const {parsed: envs} = result;


// Create a connection to the database
const connection = mysql.createConnection({
    user: envs.DB_USER,
    password: envs.DB_PASSWORD,
    host: envs.DB_HOST,
});

// open the MySQL connection
connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});

module.exports = connection;
