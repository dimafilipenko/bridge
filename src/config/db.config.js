const configs = require("./config.json")
const dotenv = require('dotenv');
const result = dotenv.config();
if (result.error) {
    throw result.error;
}
const {parsed: envs} = result;

module.exports = {
    HOST: configs[envs.env].host,
    USER: configs[envs.env].username,
    PASSWORD: configs[envs.env].password,
    DB: configs[envs.env].database
};