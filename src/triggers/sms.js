let intele = require("../controllers/intelteleController")
const axios = require('axios');
const db = require("../models");
async function sendSms(element){
    if (!element.after.sendAt || element.after.status === 'sending') {
        console.log(element.after)

        let gateway = await db.Gateways.findOne({
            where: {
                id: element.after.gateway_id
            }
        })
        let message = new intele.intelteleController()
        await message.createMessage(element.after)
        message.auth(gateway.get("auth"))
        let result = await message.sendSms(element.after.priority, element.after.system_type)
        let status
        if (result === false)
            status = 'rejected';
        else if (result === true)
            status = 'sent';
        else
            status = 'undefined';
        try {
            await db.sms.update(
                {
                    status: status
                }, {
                    where: {
                        id: element.after.id
                    }
                });
            // console.log(result)
        } catch (e) {
            console.log(e)
        }


    }

}
module.exports = (instance, MySQLEvents) => {
    instance.addTrigger({
        name: 'monitoring inserts messages',
        expression: 'app_bridge.sms', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.INSERT, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO GATEWAY
             */

            e.affectedRows.forEach(async element => {
               await sendSms(element)
            })
        }
    });
    instance.addTrigger({
        name: 'monitoring updating status',
        expression: 'app_bridge.sms.status', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.UPDATE, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO source_accounts if he have webhook_url
             */

            e.affectedRows.forEach(async element => {
                // console.log(element.after)
                if (element.after.status==='sending')
                    await sendSms(element)
                let source_account = await db.source_accounts.findOne({
                    where: {
                        id: element.after.source_account_id
                    }
                })
                if (source_account.webhook_url) {
                    axios.post(source_account.webhook_url, {
                        id: element.after.id,
                        status: element.after.status,
                        createdAt: element.after.createdAt,
                        updatedAt: element.after.updatedAt
                    }).then((res) => {
                            /**
                             * TODO SAVE info about this
                             */

                        }
                    ).catch(error => {
                        /**
                         * TODO SAVE info about this
                         */
                        console.log(error)
                    });
                }

            })
        }
    });
}