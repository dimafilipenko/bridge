let intele = require("../controllers/intelteleController")
const db = require("../models");
const axios = require('axios');

async function sendViber(element){
    if (!element.after.sendAt || element.after.status === 'sending') {

        let source_account = await db.source_accounts.findOne({
            where: {
                id: element.after.source_account_id
            }
        })
        let gateway = await db.Gateways.findOne({
            where: {
                id: element.after.gateway_id
            }
        })
        let message = new intele.intelteleController()
        await message.createMessage(element.after)
        message.auth(gateway.get("auth"))
        let result = await message.sendViber(element.after.image, element.after.button_text, element.after.button_link, element.after.ttl)
        let status;
        if (result === false)
            status = 'rejected';
        else if (result === true)
            status = 'sent';
        else
            status = 'undefined';
        try {
            await db.viber.update(
                {
                    status: status
                }, {
                    where: {
                        id: element.after.id
                    }
                });
            // console.log(result)
        } catch (e) {
            console.log(e)
        }

    }

}
module.exports = (instance, MySQLEvents) => {
    instance.addTrigger({
        name: 'monitoring inserts messages',
        expression: 'app_bridge.vibers', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.INSERT, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO GATEWAY
             */

            e.affectedRows.forEach(async element => {
               await sendViber(element)
                // console.log(element.after)
            })
        }
    });


    instance.addTrigger({
        name: 'monitoring updating status',
        expression: 'app_bridge.vibers.status', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.UPDATE, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO source_accounts if he have webhook_url
             */

                // console.log(element.after)

            e.affectedRows.forEach(async element => {
                // console.log(element.after)
                if (element.after.status==='sending')
                    await sendViber(element)
                let source_account = await db.source_accounts.findOne({
                    where: {
                        id: element.after.source_account_id
                    }
                })
                if (source_account.webhook_url) {
                    axios.post(source_account.webhook_url, {
                        id: element.after.id,
                        status: element.after.status,
                        createdAt: element.after.createdAt,
                        updatedAt: element.after.updatedAt
                    }).then((res) => {
                            /**
                             * TODO SAVE info about this
                             */

                        }
                    ).catch(error => {
                        /**
                         * TODO SAVE info about this
                         */
                        console.log(error)
                    });
                }

            })
        }
    });
}