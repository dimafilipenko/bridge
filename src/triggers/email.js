const smtp = require("../controllers/sendSmtpController")
const sendGrid = require("../controllers/sendGridCotroller")
const emailExistence = require("email-existence");
const axios = require('axios');

const db = require("../models");

async function  sendEmail(element){
    console.log(element.after)
    // let source_account = await db.source_accounts.findOne({
    //     where: {
    //         id: element.after.source_account_id
    //     }
    // })
    if (!element.after.sendAt || element.after.status==='sending') {
        let gateway = await db.Gateways.findOne({
            where: {
                id: element.after.gateway_id
            }
        })
        let result
        if (gateway.api_name === "send_grid") {
            let emailM = new sendGrid.sendGrid
            await emailM.auth(gateway.get('auth'))
            // console.log(gateway.get('auth'))
            result = await emailM.send(element.after)
        } else if (gateway.api_name === "smtp") {
            let emailM = new smtp.sendSmtp()
            result = await emailM.send(element.after)
        }
        if (result || !result) {
            console.log("REJECT MUST BE HERE")

        }

        const errHandler = err => {
            //Catch and log any error.
            console.error("Error: ", err);
        };
    }
}

module.exports = async (instance, MySQLEvents) => {
    instance.addTrigger({
        name: 'monitoring inserts messages',
        expression: 'app_bridge.emails', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.INSERT, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO GATEWAY
             */

            e.affectedRows.forEach(async element => {
                await sendEmail(element);
            })
        }
    });
    instance.addTrigger({
        name: 'monitoring updating status',
        expression: 'app_bridge.emails.status', // listen to TEST database !!!
        statement: MySQLEvents.STATEMENTS.UPDATE, // you can choose only insert for example MySQLEvents.STATEMENTS.INSERT, but here we are choosing everything
        onEvent: e => {
            /****
             *
             *SEND TO source_accounts if he have webhook_url
             */

            e.affectedRows.forEach(async element => {
                // console.log(element.after)
                if (element.after.status==='sending')
                   await sendEmail(element)
                let source_account = await db.source_accounts.findOne({
                    where: {
                        id: element.after.source_account_id
                    }
                })
                if (source_account.webhook_url) {
                    axios.post(source_account.webhook_url, {
                        id: element.after.id,
                        status: element.after.status,
                        createdAt: element.after.createdAt,
                        updatedAt: element.after.updatedAt
                    }).then((res) => {
                            /**
                             * TODO SAVE info about this
                             */
                            // console.log(res)

                        }
                    ).catch(error => {
                        /**
                         * TODO SAVE info about this
                         */
                        console.log(error)
                    });
                }

            })
        }
    });
}