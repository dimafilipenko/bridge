const inquirer = require('inquirer')
const bcrypt = require("bcrypt")
const db = require("../../src/models");

console.log('Hi, welcome to Node Pizza');

var questions = [
    {
        type: 'list',
        name: 'role',
        message: 'What role has this user?',
        choices: ['Admin', 'Default'],
        filter: function (val) {
            return val.toLowerCase();
        },
    },
    {
        type: 'input',
        name: 'name',
        message: "enter name for user",
    },
    {
        type: 'input',
        name: 'email',
        message: 'enter email',

    },
    {
        type: 'password',
        name: 'password',
        message: 'enter password',
    },


];

inquirer.prompt(questions).then(async (answers) => {
    console.log('\nOrder receipt:');
    console.log(JSON.stringify(answers, null, '  '));
    const user = db.source_accounts.build(
        {
            name: answers.name,
            role: answers.role,
            email: answers.email,
            password:answers.password,
        }
    )
// Jane exists in the database now!

    await user.save();

});