'use strict';

const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);

module.exports = async function () {
    fs.readdirSync(__dirname)
        .filter(file => {
            return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
        })
        .forEach(file => {
            const command = require(path.join(__dirname, file));
            let newCommand =new command()
            newCommand.start()
            // command.run(cron)
        });
}
