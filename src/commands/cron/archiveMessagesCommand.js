const Command = require("../../helpers/Command")
const db = require("../../models");
const moment = require("moment")

class ArchiveMessagesCommand extends Command {
    expression = "0 0 * * 0"
    run = async () => {
        console.log("HELLO CRON");
        let sms = await this.selectOldIntelTele(db.sms, 7,1)
        let viber = await this.selectOldIntelTele(db.viber, 7,1)
        let emails = await this.selectOldIntelTele(db.email, 7,1)

        if (sms.length>0)
        await this.delete(db.sms, sms)
        if (viber.length>0 )
        await this.delete(db.viber, viber)
        if (emails.length>0  )
        await this.delete(db.email, emails)

        // console.log(sms.dataValues, viber.dataValues, emails)
    }


    async selectOldIntelTele(model, from,to) {

        return await model.findAll({
            attributes: ['id', 'type', 'status'],
            where: {
                createdAt: {
                    [db.Sequelize.Op.gte]: moment().subtract(from, 'days').toDate(),
                    [db.Sequelize.Op.lte]: moment().subtract(to, 'days').toDate()

                }
            }
        })
    }

    async delete(model, messages) {
        let ids = []
        messages.forEach((message) => {
            ids.push(message.get("id"))
        })
        model.destroy({
            where: {
                id: ids
            }
        })

    }

}

module.exports = ArchiveMessagesCommand