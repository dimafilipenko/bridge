const Command = require("../../helpers/Command")
const db = require("../../models");
const moment = require("moment")

class SendMessagesByTime extends Command {
    expression = "* * * * *"
    run = async () => {
        console.log(moment().toDate())
        await this.checkToSendMessage(db.email);
        await this.checkToSendMessage(db.sms);
        await this.checkToSendMessage(db.viber);

    }


    async checkToSendMessage(model) {

        return await model.update({status: "sending"},
            {
                where: {
                    sendAt: {
                        [db.Sequelize.Op.lte]: moment().toDate()
                    },
                    status: "not_sent_yet"
                }
            }
        )
    }


}

module.exports = SendMessagesByTime