const cron = require('node-cron');
class Command {
    expression
    run
    schedule(func) {
        console.log(this.expression)
        this.run=()=>{
            func()
        }
        let cronJob = cron.schedule(this.expression, () => {
          this.run()
        });
        cronJob.start();
    }
    start() {
        this.schedule(this.run)
    }


}

module.exports = Command