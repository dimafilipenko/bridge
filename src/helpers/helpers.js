const api_keys = require('../config/api_keys');
const avaliable_api = require('../config/avaliable_api');

module.exports.check_admin_api_key = function check_admin_api_key(req, res, db) {
    if (api_keys().includes(req.body.api_key))
        return true
    else if (api_keys().includes(req.query.api_key))
        return true
    else
        res.send("error api_key")
    return false
}
module.exports.check_api_name = function check_api_name(name) {
    return avaliable_api.hasOwnProperty(name);

}
module.exports.checkRequiredOptions = function checkRequiredOptions(options, need) {
    console.log(options,need)
    let flag
    need.forEach((key) => {
        if (!options.includes(key)) {

            return flag = key;
        }
        return false
    })
    return flag
}
module.exports.check_source_accounts_api_key = function check_source_accounts_api_key(api_key, res, db) {
   if (!api_key)
       return false
    return db.source_accounts.findOne({
        where: {
            api_key: api_key
        }
    }).then((result) => {
        return !!result;
    }).catch(error => {
        return false
    })


}
module.exports.get_source_accounts_by_api_key = function get_source_accounts_by_api_key(db, api_key, callback) {
    db.source_accounts.findOne({
        where: {
            api_key: api_key
        }
    }).then(function (source_account) {
        callback(source_account)
    }).catch(error => {
        return false
    })

}
module.exports.get_gateway_by_name = function get_gateway_by_name(db, name, callback) {
    db.Gateways.findOne({
        where: {
            name: name
        }
    }).then(function (gateway) {
        callback(gateway)
    }).catch(error => {
        return false
    })
}
module.exports.formatDate = function formatDate(date) {
    let d;
    d = new Date(date)

    if (date === undefined) {
        d = new Date()
    }

    let month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}