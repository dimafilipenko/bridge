const express = require("express");
const bodyParser = require("body-parser");
const gatewayController = require("./controllers/gatewayController");
const source_accountsController = require("./controllers/source_accountsController");
const messageController = require("./controllers/messageController");
const callbacksController = require("./controllers/callbacksController");
const accountDBController = require("./controllers/accountDBController");
const cron = require('node-cron');

const scheduler=require("./commands/cron")
const db = require("../src/models");
const helmet = require('helmet')
const cors = require('cors')


const app = express();
app.use(helmet())
// parse requests of content-type: application/json
app.use(bodyParser.json({limit: '50mb'}));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({limit: '50mb',extended: true}));
app.use(cors({origin: '*'}));

/**
 * ROUTES
 */
accountDBController(app,db)
gatewayController(app, db);
source_accountsController(app, db);
messageController(app, db)
callbacksController(app, db)

app.all('*', function (req, res) {
    res.status(404).send('route not found');
});


module.exports.server = function () {
    let server = app.listen(3000, (a) => {
        let host = server.address().address;
        let port = server.address().port;
        console.log(host + ":" + port + " is started");
        scheduler();
        cron.schedule('* * * * *', function() {
            console.log('running a task every minute');
        });
    });
};